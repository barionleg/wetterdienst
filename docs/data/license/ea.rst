EA (Environment Agency)
***********************

Check out the `Terms and Conditions`_ of the Environment Agency UK for usage conditions.

.. _`Terms and Conditions`: https://support.environment.data.gov.uk/hc/en-gb/articles/360015443132-Terms-and-Conditions
